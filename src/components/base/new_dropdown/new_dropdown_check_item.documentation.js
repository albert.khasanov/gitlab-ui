import description from './new_dropdown_check_item.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-item',
};
